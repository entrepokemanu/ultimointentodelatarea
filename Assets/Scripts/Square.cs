﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

namespace Shapes
{
    class Square : Shape
    {
        public float side;

        public Square(string name, float side) : base(name)
        {
            this.side = side;
        }

        public override float Area()
        {
            float area = (float)Math.Pow(side, 2);
            //Debug.Log(name + " @ " + side + " @ " + area);
            return area;
        }
    }
}
