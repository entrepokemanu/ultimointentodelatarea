﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PKMN
{
    class Pokemon
    {
        public string name;

        public string type;

        public bool shiny;

        public Pokemon(string name)
        {
            this.name = name;
        }

        public virtual string legendario()
        {
            return "noLegendario";
        }
    }
}
