﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Circle : Shape
    {
        public float radius;

        public Circle(string name, float radius) : base(name)
        {
            this.radius = radius;
        }

        public override float Area() {
            float area = (float)(Math.PI * Math.Pow(radius, 2));
            //Debug.Log(name + " @ " + radius + " @ " + area);
            return area;
        }
    }
}
