﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;
using PKMN;

 public class Choose : MonoBehaviour
{
    public InputField nombrePokemon;

    public void ChooseYourPokemon()
    {
        string pokemonSelected = nombrePokemon.text;

        Pokemon pokemon = null;

        switch (pokemonSelected)
        {
            case "Venusaur":
                pokemon = new Venusaur( "Venusaur", 40);
                break;
            case "Blastoise":
                pokemon = new Blastoise("Blastoise", 40);
                break;
            case "Charizard":
                pokemon = new Charizard("Charizard", 40);
                break;
        }

        if (pokemon != null)
            Debug.Log(pokemon.name + " " + pokemon.legendario());
    }



 }

